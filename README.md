# React Blog

![React](http://devstickers.com/assets/img/pro/cew3.png)

A proyect build with reactjs

Read full article [here](http://engineering.devmag.io/post/1/creating-an-isomorphic-blogging-app-using-react-and-flux)

### How to run :D

1. Install Gulp - `npm install -g gulp`
2. `npm install` to install dependencies.
3. Run `gulp bundle` and `gulp watch`.
4. Run `gulp nodemon` to start the server at port 8080.
